import Nightmare from 'nightmare';
import Promise from 'bluebird';

export default function execute(User) {

  var nightmare = Nightmare({show: true});

  return new Promise((resolve, reject) => {

    return nightmare
    .goto('https://pt-attendance.nctu.edu.tw/verify/userLogin.php')
    // type account and password to login
    .type('#username', User.account)
    .type('#password', User.password)
    .click('#btnSubmit')
    // wait for the side bar
    .wait('#node_level-2-1')
    // click learning assistant
    .click('#node_level-2-1')
    // wait for the project content
    .wait('#bugetno')
    .select('#bugetno', User.project)
    .wait('#grid_grid_records')
    // use evalute to click multiple inputs
    .evaluate(() => {
      $('#grid_grid_records > table > tbody >tr input:visible').click()
    })
    // click submit buttom
    .click('#ShowWorkDetail > input[type="button"]')
    .wait(5000)
    .end(() => {
      console.log('finished');
      resolve();
    }); 

  });

}

