import Promise from 'bluebird';
import program from 'commander';
import {Converter} from 'csvtojson';
import enterLearningAssistant from './learning-assistant.js';
// import enterPartTimeAssistant from './part-time-assistant.js';

program.version('0.0.1')
.option('-t, --type [value]', 'type of assistant (learn or labour)')
.parse(process.argv);

function readCSV2JSON(fileName) {
  
  // convert csv file to json object
  var converter = new Converter({checkType: false});
  return new Promise((resolve, reject) => {
    converter.fromFile(fileName, (err, accounts) => {
      resolve(accounts);
    });
  });

}

readCSV2JSON('./user.csv')
.then((accounts) => {
  // iterate all users
  return Promise.mapSeries(accounts, (account) => {
    // login and check salary
    return enterLearningAssistant(account);
  }).catch((err) => {
    console.log(err)
  });
})
