import Nightmare from 'nightmare';
import Promise from 'bluebird';

export default function execute(User) {

  var nightmare = Nightmare({show: true});

  var mainPage = nightmare
  .goto('https://pt-attendance.nctu.edu.tw/verify/userLogin.php')
  // type account and password to login
  .type('#username', User.account)
  .type('#password', User.password)
  .click('#btnSubmit')
  .wait('#main3 > a > img')
  // click part time assistant
  .click('#node_level-1-1')
  .wait('#pno')
  .select('#pno', '104B561^90258^')
  // select work task
  .click('#form1 > button')
  .click('#ui-multiselect-workitem-option-3')
  .click('#ui-multiselect-workitem-option-6')
  .click('#form1 > button')
  .click('#CommonWords');

  mainPage
  // set start time
  .type('#datetimepicker1', '2016-04-01')
  .click('#ui-datepicker-div > table > tbody > tr:nth-child(2) > td:nth-child(3) > a')
  .click('#ui-datepicker-div > div.ui-datepicker-buttonpane.ui-widget-content > button.ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all');
  // .wait('1')

  mainPage
  // set start time
  .type('#datetimepicker2', '2016-04-01')
  .click('#ui-datepicker-div > table > tbody > tr:nth-child(2) > td:nth-child(4) > a')
  .click('#ui-datepicker-div > div.ui-datepicker-buttonpane.ui-widget-content > button.ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all')
  .wait('1');

  // mainPage.inject('js', './setHour.js');

  mainPage
  .end(() => {
    console.log('finished')
  }); 

  return;
}
