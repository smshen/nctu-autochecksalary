import moment from 'moment';
import math from 'mathjs';

function getWorkDays() {
  var now = moment();

  var validDays = [];
  // select valid days until get 5 days
  while (validDays.length < 5) {

    let randomDay = math.randomInt(1, 28);
    // ignore exist day
    if (validDays.indexOf(randomDay) > -1)
      continue;

    validDays.push(randomDay);
  }

  return validDays;

}

console.log(getWorkDays());
